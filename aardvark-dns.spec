# trust-dns-{client,server} not available
# using vendored deps

# debuginfo doesn't work yet

%global debug_package %{nil}
%global commit0 43cd18967281ab1b941f998fcbad504ce924bfa2
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name: aardvark-dns
Version: 0
Release: 0.3.git%{shortcommit0}%{?dist}
Summary: Authoritative DNS server for A/AAAA container records
License: ASL 2.0 and MIT and BSD
URL: https://github.com/containers/%{name}
Source: %{url}/archive/%{commit0}/%{name}-%{shortcommit0}.tar.gz
BuildRequires: cargo
BuildRequires: make
BuildRequires: rust-srpm-macros
ExclusiveArch: %{rust_arches}
# cargo tree --prefix none | awk '{print "Provides: bundled(crate("$1")) = "$2}' | sort | uniq
Provides: bundled(crate(aardvark-dns)) = v0.1.0
Provides: bundled(crate(aho-corasick)) = v0.7.18
Provides: bundled(crate(anyhow)) = v1.0.52
Provides: bundled(crate(async-trait)) = v0.1.52
Provides: bundled(crate(atty)) = v0.2.14
Provides: bundled(crate(autocfg)) = v1.0.1
Provides: bundled(crate(bitflags)) = v1.3.2
Provides: bundled(crate(bytes)) = v1.1.0
Provides: bundled(crate(cfg-if)) = v1.0.0
Provides: bundled(crate(chrono)) = v0.4.19
Provides: bundled(crate(clap)) = v3.0.0-beta.4
Provides: bundled(crate(clap_derive)) = v3.0.0-beta.4
Provides: bundled(crate(data-encoding)) = v2.3.2
Provides: bundled(crate(endian-type)) = v0.1.2
Provides: bundled(crate(enum-as-inner)) = v0.3.3
Provides: bundled(crate(env_logger)) = v0.8.4
Provides: bundled(crate(env_logger)) = v0.9.0
Provides: bundled(crate(form_urlencoded)) = v1.0.1
Provides: bundled(crate(futures-channel)) = v0.3.19
Provides: bundled(crate(futures-core)) = v0.3.19
Provides: bundled(crate(futures-executor)) = v0.3.19
Provides: bundled(crate(futures-io)) = v0.3.19
Provides: bundled(crate(futures-macro)) = v0.3.19
Provides: bundled(crate(futures-task)) = v0.3.19
Provides: bundled(crate(futures-util)) = v0.3.19
Provides: bundled(crate(getrandom)) = v0.2.3
Provides: bundled(crate(hashbrown)) = v0.11.2
Provides: bundled(crate(heck)) = v0.3.3
Provides: bundled(crate(humantime)) = v2.1.0
Provides: bundled(crate(idna)) = v0.2.3
Provides: bundled(crate(indexmap)) = v1.7.0
Provides: bundled(crate(instant)) = v0.1.12
Provides: bundled(crate(ipnet)) = v2.3.1
Provides: bundled(crate(lazy_static)) = v1.4.0
Provides: bundled(crate(libc)) = v0.2.112
Provides: bundled(crate(lock_api)) = v0.4.5
Provides: bundled(crate(log)) = v0.4.14
Provides: bundled(crate(matches)) = v0.1.9
Provides: bundled(crate(memchr)) = v2.4.1
Provides: bundled(crate(mio)) = v0.7.14
Provides: bundled(crate(nibble_vec)) = v0.1.0
Provides: bundled(crate(num-integer)) = v0.1.44
Provides: bundled(crate(num-traits)) = v0.2.14
Provides: bundled(crate(num_cpus)) = v1.13.1
Provides: bundled(crate(once_cell)) = v1.9.0
Provides: bundled(crate(os_str_bytes)) = v3.1.0
Provides: bundled(crate(parking_lot)) = v0.11.2
Provides: bundled(crate(parking_lot_core)) = v0.8.5
Provides: bundled(crate(percent-encoding)) = v2.1.0
Provides: bundled(crate(pin-project-lite)) = v0.2.8
Provides: bundled(crate(pin-utils)) = v0.1.0
Provides: bundled(crate(ppv-lite86)) = v0.2.16
Provides: bundled(crate(proc-macro-error)) = v1.0.4
Provides: bundled(crate(proc-macro-error-attr)) = v1.0.4
Provides: bundled(crate(proc-macro2)) = v1.0.36
Provides: bundled(crate(quote)) = v1.0.14
Provides: bundled(crate(radix_trie)) = v0.2.1
Provides: bundled(crate(rand)) = v0.8.4
Provides: bundled(crate(rand_chacha)) = v0.3.1
Provides: bundled(crate(rand_core)) = v0.6.3
Provides: bundled(crate(regex)) = v1.5.4
Provides: bundled(crate(regex-syntax)) = v0.6.25
Provides: bundled(crate(scopeguard)) = v1.1.0
Provides: bundled(crate(serde)) = v1.0.133
Provides: bundled(crate(serde_derive)) = v1.0.133
Provides: bundled(crate(signal-hook)) = v0.3.13
Provides: bundled(crate(signal-hook-registry)) = v1.4.0
Provides: bundled(crate(slab)) = v0.4.5
Provides: bundled(crate(smallvec)) = v1.7.0
Provides: bundled(crate(strsim)) = v0.10.0
Provides: bundled(crate(syn)) = v1.0.84
Provides: bundled(crate(termcolor)) = v1.1.2
Provides: bundled(crate(textwrap)) = v0.14.2
Provides: bundled(crate(thiserror)) = v1.0.30
Provides: bundled(crate(thiserror-impl)) = v1.0.30
Provides: bundled(crate(time)) = v0.1.43
Provides: bundled(crate(tinyvec)) = v1.5.1
Provides: bundled(crate(tinyvec_macros)) = v0.1.0
Provides: bundled(crate(tokio)) = v1.15.0
Provides: bundled(crate(tokio-macros)) = v1.7.0
Provides: bundled(crate(toml)) = v0.5.8
Provides: bundled(crate(trust-dns-client)) = v0.20.3
Provides: bundled(crate(trust-dns-proto)) = v0.20.3
Provides: bundled(crate(trust-dns-server)) = v0.20.3
Provides: bundled(crate(unicode-bidi)) = v0.3.7
Provides: bundled(crate(unicode-normalization)) = v0.1.19
Provides: bundled(crate(unicode-segmentation)) = v1.8.0
Provides: bundled(crate(unicode-width)) = v0.1.9
Provides: bundled(crate(unicode-xid)) = v0.2.2
Provides: bundled(crate(url)) = v2.2.2
Provides: bundled(crate(vec_map)) = v0.8.2
Provides: bundled(crate(version_check)) = v0.9.4

%description
%{summary}

Forwards other request to configured resolvers.
Read more about configuration in `src/backend/mod.rs`.

%prep
%autosetup -n %{name}-%{commit0}

%build
%{__make} build

%install
%{__make} DESTDIR=%{buildroot} PREFIX=%{_prefix} install

%files
%license LICENSE
%dir %{_libexecdir}/podman
%{_libexecdir}/podman/%{name}

%changelog
* Tue Jan 25 2022 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0-0.3.git43cd189
- use latest commit and update bundled provides

* Mon Jan 24 2022 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0-0.2.gitbab0ac1
- disabled autospec for simultaneous copr c9s build

* Mon Jan 24 2022 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0-0.1.gitbab0ac1
- initial build
